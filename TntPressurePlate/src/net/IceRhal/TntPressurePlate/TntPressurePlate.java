package net.IceRhal.TntPressurePlate;

import java.util.Iterator;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class TntPressurePlate extends JavaPlugin implements Listener {
	
	public void onEnable() {
		
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@EventHandler
	public void EntityExplode(EntityExplodeEvent e) {
		
		Iterator<Block> it = e.blockList().iterator();

		while(it.hasNext()) {
			
			Material b = it.next().getType();
			
			if(b == Material.STONE_PLATE || b == Material.WOOD_PLATE || b == Material.IRON_PLATE || b == Material.GOLD_PLATE) {
				
				it.remove();
			}
		}
	}
	
}
